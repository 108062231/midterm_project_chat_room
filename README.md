# Software Studio 2021 Spring
## Midterm Project Chat Room

### How to use 

    首先登入:

	可用Google登入或用信箱創辦帳號，會自動過濾大寫字母(信箱大小寫在firebase無區分)，而密碼必須大於
	六個字元。

    使用:

	1.右半為當前所在room的訊息，右半下方為輸入欄，輸入完成後游標點擊Submit即可送出訊息。

	2.左半為room list，顯示當前使用者加入的聊天室，上方輸入房間名稱後點擊+號即可創建新聊天室。

	3.畫面左下為使用者資料與登出鍵，游標點擊Sign out即可登出。

	4.畫面右上為menu，點擊展開有add member與exit room兩個功能，點擊add member，畫面中上會出現輸入區塊，輸入
	  欲新增的用戶信箱，點擊+號，即可新增成員至當前聊天室。而點擊exit room則會使使用者退出聊天室，設定每位使
	  用者都會被加入初始聊天室'CHAT SQUARE'，且無法退出'CHAT SQUARE'。

### Gitlab page link

    https://gitlab.com/108062231/midterm_project_chat_room

### Others (Optional)

    css的美化和rwd超花時間的啦，把基本介面和功能完成以後就沒再設計其他功能了Orz  

