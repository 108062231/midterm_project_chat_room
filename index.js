function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('signin_email');
    var txtPassword = document.getElementById('signin_pass');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function() {
        var email_val = txtEmail.value;
        var pw_val = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email_val, pw_val).then(function(result) {
            window.location.href = "signed.html";}
        ).catch(function(error) {
            create_alert("error", error.message);
          });
    });

    btnGoogle.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithPopup(provider).then(function(result) {
            var email_val = result.user.email;
            var userRef =firebase.database().ref("user_list/"+email_val.replace(/[`~!@#$&*()=|{}:;,\[\].<>/?~！@#￥……&*（）&;|{}【】‘；：”“。，、？""]/g,'_')+"/room");
            var new_user = true;
            userRef.once('value').then(function(snapshot) {
                var alldata = snapshot.val();
                for(var key in alldata){
                    if(alldata[key].room_name == 'CHAT SQUARE'){
                        new_user =false;
                    }
                } 
                if(new_user){
                    userRef.push({room_name:'CHAT SQUARE'}).then(()=>{
                        firebase.database().ref('room_list/'+'CHAT SQUARE'+'/com_list').push({
                            sender: 'system',
                            data : "Welcome to "+'CHAT SQUARE'+ "," + email_val + "!"
                    }).then(()=>{
                        window.location.href = "signed.html";
                        txtEmail.value = "";
                        txtPassword.value = "";})
                    });
                }else{window.location.href = "signed.html";txtEmail.value = "";txtPassword.value = "";}
            })
            
          }).catch(function(error) {
            create_alert("error", error.message);
          });
    });

    btnSignUp.addEventListener('click', function() {
        var email_val = txtEmail.value;
        var pw_val = txtPassword.value;
        console.log(email_val);
        firebase.auth().createUserWithEmailAndPassword(email_val, pw_val).then(function(result) {
            firebase.database().ref("user_list/"+email_val.replace(/[`~!@#$&*()=|{}:;,\[\].<>/?~！@#￥……&*（）&;|{}【】‘；：”“。，、？""]/g,'_')+"/room").push({
                room_name:'CHAT SQUARE'
            }).then(firebase.database().ref('room_list/'+'CHAT SQUARE'+'/com_list').push({
                sender: 'system',
                data : "Welcome to "+'CHAT SQUARE'+ "," + email_val + "!"
            })).then(()=>{window.location.href = "signed.html";txtEmail.value = "";txtPassword.value = "";}); 
        }).catch(function(error) {
            create_alert("error", error.message);
          });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        var str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        var str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};

function output(data){
    console.log(data);
}
