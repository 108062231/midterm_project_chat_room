var this_room = 'CHAT SQUARE';
var postsRef = firebase.database().ref('room_list/'+this_room+'/com_list');
var user_email ='';
function init() {
        firebase.auth().onAuthStateChanged(function(user) {
            var user_info = document.getElementById('user_email');
            var user_logo = document.getElementById('email_logo');
            user_email = user.email;
            user_logo.innerHTML = user.email.charAt(0);
            user_info.innerHTML =  user.email ;
            var LogoutBtn = document.getElementById("logout-btn");
            LogoutBtn.addEventListener("click", () => {
                firebase.auth().signOut().then( () => {
                    alert("Logout Successfully!");
                    window.location.href = "index.html";
                })
            }) 

                    // List for store posts html
        var total_room = [];
        
        user_path = user_email.replace(/[`~!@#$&*()=|{}:;,\[\].<>/?~！@#￥……&*（）&;|{}【】‘；：”“。，、？""]/g,'_');
        roomRef = firebase.database().ref("user_list/"+user_path+"/room");
            roomRef.on('value', function(snapshot) {
                var alldata = snapshot.val();total_room.push("<br>");
                for(var key in alldata){
                    var content="";
                    content = "<li class='person' onclick='changeRoom(this.id)' id='"+ alldata[key] .room_name+"'><span class='title'>" + alldata[key] .room_name+ "</span></li>";
                    total_room.push(content);
                }
                document.getElementById('room_list').innerHTML = total_room.join('');
                total_room = [];
            })
        })

    
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('text_su');

    post_btn.addEventListener('click', function() {
        
        if (post_txt.value != "") {
            firebase.database().ref('room_list/'+this_room+'/com_list').push({
                "sender": user_email,
                "data" : post_txt.value
            });
            post_txt.value = "";
        }
    });
                
    add_room_btn = document.getElementById('addRoom_btn');
    room_name = document.getElementById('room_su');

    add_room_btn.addEventListener('click', function() {
        
        if (room_name.value != "" && room_name.value != "CHAT SQUARE") {
            firebase.database().ref('room_list/'+room_name.value+'/com_list').push({
                sender: 'system',
                data : "Welcome to "+room_name.value+ "," + user_email + "!"
            });
            firebase.database().ref("user_list/"+user_path+"/room").push({
                room_name:room_name.value
            })
            room_name.value = "";
        }else if(room_name.value == 'CHAT SQUARE'){
            create_alert("error","Can't creat a room name 'CHAT SQUARE'!");
            room_name.value = "";
        }
    });

    
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            var alldata = snapshot.val();
            total_post.push("<br>");
            for(var key in alldata){
                first_count++;
                var content="";
                if(alldata[key].sender==user_email){
                    content = "<div class='bubble' id='outgoing'>" + "<div class='uesr_name'>" + alldata[key].sender+":</div><br>"  + alldata[key].data + "</div><p><br></p>";
                }else{
                    content = "<div class='bubble' id='incoming'>" + "<div class='uesr_name'>" + alldata[key].sender+":</div><br>"  + alldata[key].data + "</div><p><br></p>";  
                }
                for(var i =alldata[key].data.length;i>0;i-=50){
                    content =content+"<br>";
                }
                total_post.push(content);
            }
            var element = document.getElementById("post_list");
            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');
            element.scrollTop = element.scrollHeight;
            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var content="";
                    if(childData.sender==user_email){
                        content = "<div class='bubble' id='outgoing'>" + "<div class='uesr_name'>" + childData.sender+":</div><br>"  + childData.data + "</div><p><br></p>";
                    }else{
                        content = "<div class='bubble' id='incoming'>" + "<div class='uesr_name'>" + childData.sender+":</div><br>"  + childData.data + "</div><p><br></p>";
                    }
                    for(var i =childData.data.length;i>0;i-=50){
                        content =content+"<br>";
                    }
                    total_post.push(content);
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    element.scrollTop = element.scrollHeight;
                }
            });
        })
        .catch(e => console.log(e.message));
}

function changeRoom(room_id){
    this_room = room_id;
    postsRef = firebase.database().ref('room_list/'+this_room+'/com_list');
    document.getElementById('room_title').innerHTML = this_room;
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
    .then(function(snapshot) {
        var alldata = snapshot.val();
        total_post.push("<br>");
        for(var key in alldata){
            first_count++;
            var content="";
            if(alldata[key].sender==user_email){
                content = "<div class='bubble' id='outgoing'>" + "<div class='uesr_name'>" + alldata[key].sender+":</div><br>"  + alldata[key].data + "</div><p><br></p>";
            }else{
                content = "<div class='bubble' id='incoming'>" + "<div class='uesr_name'>" + alldata[key].sender+":</div><br>"  + alldata[key].data + "</div><p><br></p>";  
            }
            for(var i =alldata[key].data.length;i>0;i-=50){
                content =content+"<br>";
            }
            total_post.push(content);
        }
        var element = document.getElementById("post_list");
        /// Join all post in list to html in once
        document.getElementById('post_list').innerHTML = total_post.join('');
        element.scrollTop = element.scrollHeight;
        /// Add listener to update new post
        postsRef.on('child_added', function(data) {
            second_count += 1;
            if (second_count > first_count) {
                var childData = data.val();
                var content="";
                if(childData.sender==user_email){
                    content = "<div class='bubble' id='outgoing'>" + "<div class='uesr_name'>" + childData.sender+":</div><br>"  + childData.data + "</div><p><br></p>";
                }else{
                    content = "<div class='bubble' id='incoming'>" + "<div class='uesr_name'>" + childData.sender+":</div><br>"  + childData.data + "</div><p><br></p>";
                }
                for(var i =childData.data.length;i>0;i-=50){
                    content =content+"<br>";
                }
                total_post.push(content);
                document.getElementById('post_list').innerHTML = total_post.join('');
                element.scrollTop = element.scrollHeight;
            }
        });
    })
    .catch(e => console.log(e.message));
}

window.onload = function() {
    init();
};

function addMember_start(){
    document.getElementById("member_btn").style.display= "block";
    document.getElementById("member_logo").style.display= "block";
    document.getElementById("member_su").style.display= "block";
    document.getElementById("member_close").style.display= "block";
}

function addMember_close(){
    document.getElementById("member_btn").style.display= "none";
    document.getElementById("member_logo").style.display= "none";
    document.getElementById("member_su").style.display= "none";
    document.getElementById("member_close").style.display= "none";
}
function addMember_done(){
    document.getElementById("member_btn").style.display= "none";
    document.getElementById("member_logo").style.display= "none";
    document.getElementById("member_su").style.display= "none";
    document.getElementById("member_close").style.display= "none";
    var memberID = document.getElementById('member_su');
    if(memberID.value != '' && this_room != 'CHAT SQUARE'){
        firebase.database().ref("user_list/"+memberID.value.replace(/[`~!@#$&*()=|{}:;,\[\].<>/?~！@#￥……&*（）&;|{}【】‘；：”“。，、？""]/g,'_')+"/room").push({
            room_name:this_room
        })
        firebase.database().ref('room_list/'+this_room+'/com_list').push({
            sender: 'system',
            data : "Welcome to "+this_room+ "," + memberID.value + "!"
        });
    }else if(this_room == 'CHAT SQUARE'){
        create_alert("error","Can't add member to CHAT SQUARE!");
    }
    memberID.value='';
}

function exitRoom(){
    var userRoom = firebase.database().ref("user_list/"+user_email.replace(/[`~!@#$&*()=|{}:;,\[\].<>/?~！@#￥……&*（）&;|{}【】‘；：”“。，、？""]/g,'_')+"/room");
    userRoom.once('value').then(function(snapshot) {
        var alldata = snapshot.val();
        for(var key in alldata){
            console.log(alldata[key].room_name);
            if(alldata[key].room_name == this_room && this_room!= 'CHAT SQUARE'){
                userRoom.child(key).remove();
                changeRoom('CHAT SQUARE');
            }else{
                if(this_room == 'CHAT SQUARE'){
                    create_alert("error","Can't exit CHAT SQUARE!");
                }
            }
        }  
    })
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert-2');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}